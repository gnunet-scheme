dnl Process this file with autoconf to produce a configure script.
# Copyright © 2000-2012, 2016, 2019 Free Software Foundation, Inc.
# Copyright © 2020-2022 GNUnet e.V.
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Author (GnuTLS): Nikos Mavrogiannopoulos, Simon Josefsson
# Author (scheme-GNUnet): Maxime Devos
#
# This file is part of scheme-gnunet
#
# scheme-GNUnet is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# scheme-GNUnet is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with scheme-GNUnet.  If not, see <http://www.gnu.org/licenses/>.

# This file is partially based upon the Autoconf script of GnuTLS.
# Not all scheme-GNUnet code is licensed as GPL, most is licensed as AGPL.

AC_PREREQ([2.61])
AC_INIT([scheme-gnunet], [0.2], [bug-gnunet@gnu.org])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIRS([m4])

AM_INIT_AUTOMAKE([foreign subdir-objects])

AC_CANONICAL_HOST

# Don't use $(GUILE_SITE) and $(GUILE_SITE_CCACHE), that doesn't work on Guix and Nix and the like.
AC_ARG_WITH([guile-site-dir], AS_HELP_STRING([--with-guile-site-dir=DIR],
    [guile site directory for scheme-gnunet]),
    [guilesitedir="${withval}"], [guilesitedir='$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)'])
AC_ARG_WITH([guile-site-ccache-dir], AS_HELP_STRING([--with-guile-site-ccache-dir=DIR],
    [guile ccache directory for scheme-gnunet]),
    [guilesiteccachedir="${withval}"], [guilesiteccachedir='$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache'])
AC_SUBST([guilesitedir])
AC_SUBST([guilesiteccachedir])

dnl Check for 'guild', which can be used to compile Scheme code
AC_PATH_PROG([GUILD], [guild])
AC_SUBST([GUILD])

GUILE_PKG([3.0])
GUILE_PROGS

dnl Test for required Guile libraries
GUILE_MODULE_REQUIRED([fibers])
GUILE_MODULE_REQUIRED([pfds queues])
GUILE_MODULE_REQUIRED([gcrypt hash])

dnl Not yet upstreamed at time of writing
GUILE_MODULE_REQUIRED([fibers io-wakeup])

AC_OUTPUT([Makefile])
