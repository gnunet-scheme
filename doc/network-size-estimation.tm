<TeXmacs|2.1>

<project|scheme-gnunet.tm>

<style|tmmanual>

<\body>
  <index|network size estimation><index|NSE>GNUnet has a service that roughly
  estimates the size of the network \U i.e., the number of peers. The module
  <scm|(gnu gnunet nse client)><index|(gnu gnunet nse client)> can be used to
  interact with this service, this module implements the
  <em|<reference|server object>> pattern. The procedure
  <scm|connect><subindex|connect|NSE><label|nse:connect> accepts one
  additional optional keyword argument: <scm|#:updated <var|updated>>. As
  usual, it returns a <dfn|NSE server object><index|NSE server
  object><subindex|server object|NSE>. As usual,
  <scm|disconnect!><subindex|disconnect!|NSE> can be called on the server
  object to disconnect.

  Whenever a new estimate becomes available, the (optional) procedure
  <var|updated><index|update procedure> is called with the new
  <with|font-shape|italic|estimate><index|estimate
  object>.<space|1em>Alternatively, the procedure
  <scm|estimate><index|estimate> can be called on the server object to return
  the latest available estimate.<space|1em>If the <em|NSE server object>
  doesn't have an estimate yet, that procedure will return <scm|#false>
  instead of an estimate. As usual, the idempotent procedure
  <scm|disconnect!><subindex|disconnect!|NSE>

  <todo|input, validation, I/O errors?>

  The estimate object has a number of accessors:

  <\explain>
    <scm|(estimate:logarithmic-number-peers
    <var|estimate>)><index|estimate:logarithmic-number-peers>
  </explain|The base-2 logarithm of the number of peers (estimated), as a
  positive flonum, possibly zero or infinite>

  <\explain>
    <scm|(estimate:number-peers <var|estimate>)><index|estimate:number-peeers>
  </explain|The number of peers (estimated), as a flonum, at least <scm|1.0>
  and possibly infinite.<space|1em>This is not necessarily an (inexact)
  <scm|integer?>, as it is only an estimate.>

  <\explain>
    <scm|(estimate:timestamp estimate)><index|estimate:timestamp>
  </explain|A timestamp when the estimate was made <todo|something about
  epoch?>>

  <\explain>
    <scm|(estimate:standard-deviation <var|estimate>)><index|estimate:standard-deviation>
  <|explain>
    The estimated standard deviation on the base-2 logarithm of peers,
    calculated over the last 64 rounds, with the <math|<frac|N|N-1>>
    correction.<space|1em>This is a positive flonum, possibly zero or
    infinite, or not-a-number (indicating division of zero by zero).

    If the peer has been connected to the network for a while, the number is
    expected to be finite and strictly positive.
  </explain>

  Assuming the network size is stable and the errors on the logarithmic
  estimate are normally distributed, the procedure
  <scm|estimate:standard-deviation> can be used to put probablistic error
  bounds on the number of peers on the network. <todo|example>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|save-aux|false>
  </collection>
</initial>