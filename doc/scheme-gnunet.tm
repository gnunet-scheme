<TeXmacs|2.1>

<style|<tuple|tmmanual|british|doc>>

<\body>
  <doc-data|<doc-title|Scheme-GNUnet (0.3)
  manual>|<doc-author|<author-data|<author-name|Maxime
  Devos>|<author-email|maximedevos@telenet.be>>>>

  Copyright \<#A9\> 2012-2016, 2021\U2023 GNUnet e.V.

  Permission is granted to copy, distribute and/or modify this document under
  the terms of the GNU Free Documentation License, Version 1.3 or any later
  version published by the Free Software Foundation; \ with no Invariant
  Sections, no Front-Cover Texts, and no Back-Cover Texts.<space|1em>A copy
  of the license is included in the section entitled \<#2018\>GNU Free
  Documentation License\<#2019\>.

  \;

  <\table-of-contents|toc>
    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|1.<space|2spc>Installation
    and contributing guide> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-1><vspace|0.5fn>

    1.1.<space|2spc>Building from source <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-2>

    <with|par-left|1tab|1.1.1.<space|2spc>Authenticating new source code
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-4>>

    1.2.<space|2spc>Writing tests <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-6>

    1.3.<space|2spc>Writing portable Scheme code
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-8>

    1.4.<space|2spc>Contact <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-9>

    1.5.<space|2spc>License <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-10>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|2.<space|2spc>Application
    guide> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-11><vspace|0.5fn>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|3.<space|2spc>Concurrency>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-12><vspace|0.5fn>

    <with|par-left|4tab|Repeated conditions
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-13><vspace|0.15fn>>

    3.1.<space|2spc>Waiting for unreachability of objects
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-14>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|4.<space|2spc>Configuration>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-20><vspace|0.5fn>

    4.1.<space|2spc>Locating configuration files
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-29>

    4.2.<space|2spc>Loading configuration files
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-37>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|5.<space|2spc>Manipulation
    of network structures> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-42><vspace|0.5fn>

    5.1.<space|2spc>Documentation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-48>

    5.2.<space|2spc>Reading and writing <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-53>

    5.3.<space|2spc>Primitive types <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-58>

    5.4.<space|2spc>Packing <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-59>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|6.<space|2spc>Typeclasses
    \U common patterns> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-60><vspace|0.5fn>

    6.1.<space|2spc>Server objects <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-61>

    6.2.<space|2spc>Server-associated objects
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-66>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|7.<space|2spc>Communication
    with services> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-67><vspace|0.5fn>

    7.1.<space|2spc>Asynchronuously connecting
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-72>

    7.2.<space|2spc>Message handler <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-76>

    7.3.<space|2spc>Message type database
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-84>

    7.4.<space|2spc>Sending messages <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-90>

    7.5.<space|2spc>Error handler <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-92>

    7.6.<space|2spc>Ordering of injected errors and messages and sent
    messages <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-103>

    7.7.<space|2spc>Disconnecting <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-104>

    7.8.<space|2spc>Error reporting <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-108>

    7.9.<space|2spc>Testing service code <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-114>

    7.10.<space|2spc>Writing service communication code
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-115>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|8.<space|2spc>Estimation
    of the size of the network> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-122><vspace|0.5fn>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|9.<space|2spc>Accessing
    the DHT> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-138><vspace|0.5fn>

    9.1.<space|2spc>Data in the DHT <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-147>

    9.2.<space|2spc>Accessing data in the DHT
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-179>

    9.3.<space|2spc>Constructing and analysing network messages
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-190>

    9.4.<space|2spc>How to handle invalid data
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-208>

    9.5.<space|2spc>Monitoring: spying on what other applications and peers
    are doing <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-209>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|10.<space|2spc>Communication
    between peers> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-210><vspace|0.5fn>

    10.1.<space|2spc>Qualities and limitations \V avoiding reinventing the
    wheel <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-216>

    10.2.<space|2spc>Addresses <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-218>

    10.3.<space|2spc>Listening at an address
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-221>

    10.4.<space|2spc>Connecting to an address
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-222>

    10.5.<space|2spc>Performing I/O \U GNUnet style
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-226>

    10.6.<space|2spc>Performing I/O \U BSD style
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-227>

    10.7.<space|2spc>BSD socket integration
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-228>

    10.8.<space|2spc>Constructing and analysing network messages
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-229>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|11.<space|2spc>Miscellaneous>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-238><vspace|0.5fn>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|Appendix
    A.<space|2spc>GNU Free Documentation License>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-251><vspace|0.5fn>

    0. <with|font-shape|small-caps|Preamble>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-252>

    1. <with|font-shape|small-caps|Applicability and definitions>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-253>

    2. <with|font-shape|small-caps|Verbatim copying>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-254>

    3. <with|font-shape|small-caps|Copying in quantity>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-255>

    4. <with|font-shape|small-caps|Modifications>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-256>

    5. <with|font-shape|small-caps|Combining documents>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-257>

    6. <with|font-shape|small-caps|Collections of documents>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-258>

    7. <with|font-shape|small-caps|Aggregation with independent works>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-259>

    8. <with|font-shape|small-caps|Translation>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-260>

    9. <with|font-shape|small-caps|Termination>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-261>

    10. <with|font-shape|small-caps|Future revisions of this license>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-262>

    11. <with|font-shape|small-caps|Relicensing>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-263>

    <with|font-shape|small-caps|Addendum>: How to use this License for your
    documents <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <no-break><pageref|auto-264>

    <vspace*|1fn><with|font-series|bold|math-font-series|bold|font-shape|small-caps|Index>
    <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
    <pageref|auto-265><vspace|0.5fn>
  </table-of-contents>

  <chapter|Installation and contributing guide>

  <include|contributing.tm>

  <chapter|Application guide>

  Scheme-GNUnet doesn't have any example applications, except the half-baked
  <verbatim|examples/web.scm>, <verbatim|gnu/gnunet/scripts/download-store.scm>
  and <verbatim|gnu/gnunet/scripts/publish-store.scm>.<space|1em>Over time,
  we hope we have something to write here, but for now, this chapter is
  empty.

  <chapter|Concurrency>

  <include|concurrency.tm>

  <chapter|Configuration>

  <include|configuration.tm>

  <chapter|Manipulation of network structures>

  <include|network-structures.tm>

  <chapter|Typeclasses \U common patterns>

  <include|typeclasses.tm>

  <chapter|Communication with services>

  <include|service-communication.tm>

  <chapter|Estimation of the size of the network>

  <include|network-size-estimation.tm>

  <chapter|Accessing the DHT>

  <include|distributed-hash-table.tm>

  <chapter|Communication between peers>

  <include|cadet.tm>

  <chapter|Miscellaneous>

  The module <scm|(gnu gnunet crypto)><index|(gnu gnunet crypto)> has a few
  small wrappers around procedures from Guile-Gcrypt for performing
  cryptography on bytevector slices.

  <\explain>
    <scm|(hash/sha512! <var|slice> <var|to>)><index|hash/sha512!>
  </explain|Compute the SHA-512 hash of <var|slice>, a readable bytevector
  slice of arbitrary length, and write it to <var|to>, a writable bytevector
  slice of length 512 bits / 64 bytes.<space|1em>The result is unspecified if
  <var|slice> and <var|to> overlap.>

  <\explain>
    <scm|(hash/sha512 <var|slice>)><index|hash/sha512>
  <|explain>
    Like <scm|hash/sha512!>, but allocate the <var|>destination slice
    <var|to> and return it.
  </explain>

  It also has a procedure <scm|string-\<gtr\>eddsa-public-key><index|string-\<gtr\>eddsa-public-key>
  and <scm|string-\<gtr\>ecdsa-public-key><index|string-\<gtr\>ecdsa-public-key>.
  They respectively accept a Crockford Base32-encoded EdDSA and ECDSA public
  key as a string and return the decoded key as a fresh bytevector. In case
  the string has an incorrect length or characters, a
  <scm|&invalid-public-key-encoding><index|&invalid-public-key-encoding>
  condition is raised, which can be tested for with the predicate
  <scm|invalid-public-key-encoding?><index|invalid-public-key-encoding?>.

  The module <scm|(gnu gnunet data-string)><index|(gnu gnunet data-string)>
  has two procedures <scm|data-\<gtr\>string><index|data-\<gtr\>string> and
  <scm|string-\<gtr\>data><index|string-\<gtr\>data>, for representing binary
  data in ASCII with the Crockford Base32 encoding <todo|TODO: convert to
  slices and document>. They correspond with the
  <cpp|GNUNET_STRINGS_data_to_string><index|GNUNET_STRINGS_data_to_string>
  and <cpp|GNUNET_STRINGS_string_to_data><index|GNUNET_STRINGS_string_to_data>
  functions in the C implementation.

  <section|Hashcodes>

  In various places in GNUnet, and consequently Scheme-GNUnet,
  <dfn|hashcodes><index|hashcode> are used. A hashcode is the SHA-512 hash of
  some value, this way a hashcode identifies a value, up to (hopefully very
  unlikely) hash collisions. There are two types of hashcodes in use in
  GNUnet: <dfn|short hashcodes><index|short hashcode> and <dfn|long
  hashcodes><index|long hashcode>, respectively of length 256 bits/32 bytes
  and 512 bits/64 bytes. In Scheme-GNUnet, these are named
  <dfn|hashcode:256><index|hashcode:256> and
  <dfn|hashcode:512><index|hashcode:512> instead.

  There are two kind of representations hashcodes: the record types
  <scm|\<less\>hashcode:512\<gtr\>><index|\<less\>hashcode:512\<gtr\>> and
  <scm|\<less\>hashcode:256\<gtr\>><index|\<less\>hashcode:256\<gtr\>> from
  <scm|(gnu gnunet hashcode)><index|(gnu gnunet hashcode)> and the
  corresponding <reference|network types>
  <scm|/hashcode:512><index|/hashcode:521> and
  <scm|/hashcode:512><index|/hashcode:256> from <scm|(gnu gnunet hashcode
  struct)><index|(gnu gnunet hashcode struct)>. The module <scm|(gnu gnunet
  hashcode struct)> is documented below.

  <\explain>
    <scm|(make-hashcode:512/share <var|slice>)><index|make-hashcode:512/share>
  <|explain>
    Make a <scm|hashcode:512> containing <var|slice> (a readable
    <scm|/hashcode:512> bytevector slice). <var|slice> may not be mutated
    while the constructed hashcode is in use. The contents can be retrieved
    with <scm|hashcode:512-\<gtr\>slice>, as a readable <scm|/hashcode:512>
    bytevector slice. It can be tested if an object is a <scm|hashcode:512>
    with the predicate <scm|hashcode:512?>.
  </explain>

  <\explain>
    <scm|(make-hashcode:256/share <var|slice>)><index|make-hashcode:256/share>
  <|explain>
    Make a <scm|hashcode:256> containing <var|slice> (a readable
    <scm|/hashcode:256> bytevector slice). <var|slice> may not be mutated
    while the constructed hashcode is in use. The contents can be retrieved
    with <scm|hashcode:256-\<gtr\>slice>, as a readable <scm|/hashcode:256>
    bytevector slice. It can be tested if an object is a <scm|hashcode:256>
    with the predicate <scm|hashcode:256?>.
  </explain>

  <\explain>
    <scm|(make-hashcode:512 <var|slice>)><index|make-hashcode:512>

    <scm|(make-hashcode:256 <var|slice>)><index|make-hashcode:256>
  </explain|This is equivalent to <scm|(make-hashcode:512 slice)> and
  <scm|(make-hashcode:256 slice)>, except that it doesn't assume that
  <var|slice> is not mutated. However, this imposes a small cost, as
  <var|slice> will then be copied behind the scenes.>

  The module <scm|(gnu gnunet hashcode quickcheck)><index|(gnu gnunet
  hashcode quickcheck)> defines a few <em|arbitraries> for use with
  Guile-Quickcheck:

  <\explain>
    <scm|$hashcode:512><index|$hashcode:512>,
    <scm|$hashcode:256><index|$hashcode:256>
  </explain|Arbitraries generating hashcode:512 and hashcode:256
  respectively.>

  <include|bytevector-slices.tm>

  <appendix|GNU Free Documentation License>

  <include|fdl.tm>

  <\the-index|idx>
    <index+1|analyse-client-get|<pageref|auto-201>>

    <index+1|analyse-client-get-stop|<pageref|auto-203>>

    <index+1|analyse-client-put|<pageref|auto-204>>

    <index+1|analyse-client-result|<pageref|auto-206>>

    <index+1|analysis procedures|<pageref|auto-192>>

    <index+1|authentication|<pageref|auto-5>>

    <index+1|CADET address|<pageref|auto-220>>

    <index+1|CADET|<pageref|auto-211>>

    <index+1|channel-message-queue|<pageref|auto-224>>

    <index+1|close-channel!|<pageref|auto-225>>

    <index+1|close-queue!|<pageref|auto-106>>

    <index+1|~/.config/gnunet.conf|<pageref|auto-34>>

    <index+1|configuration|<pageref|auto-21>>

    <index+2|configuration|system|<pageref|auto-31>>

    <index+2|configuration|user|<pageref|auto-30>>

    <index+1|connect|<pageref|auto-63>>

    <index+2|connect|CADET|<pageref|auto-213>>

    <index+2|connect|DHT|<pageref|auto-143>>

    <index+2|connect|NSE|<pageref|auto-126>>

    <index+1|connect/fibers|<pageref|auto-69>>

    <index+1|connecting to services|<pageref|auto-73>>

    <index+1|connection:connected|<pageref|auto-75>, <pageref|auto-96>>

    <index+1|connection:interrupted|<pageref|auto-97>, <pageref|auto-107>>

    <index+1|construct-client-get|<pageref|auto-194>>

    <index+1|construct-client-get-stop|<pageref|auto-196>>

    <index+1|construct-client-put|<pageref|auto-197>>

    <index+1|construct-client-result|<pageref|auto-199>>

    <index+1|construct-close-local-port|<pageref|auto-232>>

    <index+1|construction procedures|<pageref|auto-193>>

    <index+1|construct-local-acknowledgement|<pageref|auto-236>>

    <index+1|construct-local-data|<pageref|auto-234>>

    <index+1|construct-open-local-port|<pageref|auto-230>>

    <index+1|control channel|<pageref|auto-116>>

    <index+1|copy-datum|<pageref|auto-187>>

    <index+1|copy-insertion|<pageref|auto-188>>

    <index+1|copy-query|<pageref|auto-186>>

    <index+1|copy-search-result|<pageref|auto-189>>

    <index+1|data-\<gtr\>string|<pageref|auto-247>>

    <index+1|datum?|<pageref|auto-156>>

    <index+1|datum object|<pageref|auto-148>>

    <index+1|datum-expiration|<pageref|auto-155>>

    <index+1|datum-\<gtr\>insertion|<pageref|auto-159>>

    <index+1|datum-\<gtr\>search-result|<pageref|auto-168>>

    <index+1|datum-key|<pageref|auto-153>>

    <index+1|datum-value|<pageref|auto-154>>

    <index+1|define-type|<pageref|auto-45>>

    <index+1|DHT server object|<pageref|auto-145>>

    <index+1|DHT|<pageref|auto-140>>

    <index+1|/dht:path-element|<pageref|auto-171>>

    <index+1|disconnect!|<pageref|auto-65>, <pageref|auto-121>>

    <index+2|disconnect!|CADET|<pageref|auto-215>>

    <index+2|disconnect!|DHT|<pageref|auto-144>>

    <index+2|disconnect!|NSE|<pageref|auto-129>, <pageref|auto-133>>

    <index+1|disconnecting|<pageref|auto-105>>

    <index+1|distributed hash table|<pageref|auto-139>>

    <index+1|documentation|<pageref|auto-50>>

    <index+1|error handler|<pageref|auto-93>>

    <index+1|error reporting|<pageref|auto-109>>

    <index+1|error-handler|<pageref|auto-74>>

    <index+1|error-reporter|<pageref|auto-113>>

    <index+1|estimate|<pageref|auto-132>>

    <index+1|estimate object|<pageref|auto-131>>

    <index+1|estimate:logarithmic-number-peers|<pageref|auto-134>>

    <index+1|estimate:number-peeers|<pageref|auto-135>>

    <index+1|estimate:standard-deviation|<pageref|auto-137>>

    <index+1|estimate:timestamp|<pageref|auto-136>>

    <index+1|/etc/gnunet.conf|<pageref|auto-36>>

    <index+1|found|<pageref|auto-17>>

    <index+1|get path|<pageref|auto-173>>

    <index+1|(gnu extractor enum)|<pageref|auto-88>>

    <index+1|(gnu gnunet cadet client)|<pageref|auto-212>>

    <index+1|(gnu gnunet concurrency lost-and-found)|<pageref|auto-15>>

    <index+1|(gnu gnunet config db)|<pageref|auto-22>>

    <index+1|(gnu gnunet config fs)|<pageref|auto-32>>

    <index+1|(gnu gnunet crypto)|<pageref|auto-239>>

    <index+1|(gnu gnunet data-string)|<pageref|auto-246>>

    <index+1|(gnu gnunet dht client)|<pageref|auto-142>>

    <index+1|(gnu gnunet dht network)|<pageref|auto-191>>

    <index+1|(gnu gnunet message protocols)|<pageref|auto-86>>

    <index+1|(gnu gnunet mq error-reporting)|<pageref|auto-110>>

    <index+1|(gnu gnunet mq-impl stream)|<pageref|auto-70>>

    <index+1|(gnu gnunet nse client)|<pageref|auto-125>>

    <index+1|GNUNET_STRINGS_data_to_string|<pageref|auto-249>>

    <index+1|GNUNET_STRINGS_string_to_data|<pageref|auto-250>>

    <index+1|Guix|<pageref|auto-3>>

    <index+1|handler procedure|<pageref|auto-83>>

    <index+1|hash-\<gtr\>configuration|<pageref|auto-23>>

    <index+1|hash-key|<pageref|auto-24>>

    <index+1|hash/sha512!|<pageref|auto-240>>

    <index+1|hash/sha512|<pageref|auto-241>>

    <index+1|inject-error!|<pageref|auto-94>>

    <index+1|input:overly-small|<pageref|auto-100>>

    <index+1|input:premature-end-of-file|<pageref|auto-99>>

    <index+1|input:regular-end-of-file|<pageref|auto-98>>

    <index+1|inserting data into the DHT|<pageref|auto-181>>

    <index+1|insertion?|<pageref|auto-162>>

    <index+1|insertion object|<pageref|auto-149>>

    <index+1|insertion-desired-replication-level|<pageref|auto-161>>

    <index+1|insertion-\<gtr\>datum|<pageref|auto-160>>

    <index+1|interposer|<pageref|auto-81>>

    <index+1|&invalid-public-key-encoding|<pageref|auto-244>>

    <index+1|invalid-public-key-encoding?|<pageref|auto-245>>

    <index+1|key=?|<pageref|auto-25>>

    <index+1|key|<pageref|auto-95>>

    <index+1|\<less\>losable\<gtr\>|<pageref|auto-19>>

    <index+1|\<less\>server\<gtr\>|<pageref|auto-119>>

    <index+1|load-configuration|<pageref|auto-41>>

    <index+1|load-configuration/port!|<pageref|auto-38>>

    <index+1|locate-system-configuration|<pageref|auto-35>>

    <index+1|locate-user-configuration|<pageref|auto-33>>

    <index+1|logic:ill-formed|<pageref|auto-102>>

    <index+1|logic:no-handler|<pageref|auto-101>>

    <index+1|lost|<pageref|auto-16>>

    <index+1|lost-and-found|<pageref|auto-18>>

    <index+1|make-datum|<pageref|auto-152>>

    <index+1|make-disconnect!|<pageref|auto-120>>

    <index+1|make-expanded-configuration|<pageref|auto-40>>

    <index+1|make-message-handler|<pageref|auto-79>>

    <index+1|make-query|<pageref|auto-163>>

    <index+1|&malformed-path|<pageref|auto-178>>

    <index+1|%max-datum-value-length|<pageref|auto-157>>

    <index+1|maybe-sending|<pageref|auto-117>>

    <index+1|message handler|<pageref|auto-77>>

    <index+1|message queue|<pageref|auto-71>>

    <index+1*|message type>

    <index+2|message type|database|<pageref|auto-85>>

    <index+2|message type|of handler|<pageref|auto-80>>

    <index+1|message-handler|<pageref|auto-78>>

    <index+1|message-symbol|<pageref|auto-52>>

    <index+1|/:msg:cadet:local:acknowledgemeent|<pageref|auto-237>>

    <index+1|/:msg:cadet:local:data|<pageref|auto-235>>

    <index+1|/:msg:cadet:local:port:close|<pageref|auto-233>>

    <index+1|/:msg:cadet:local:port:open|<pageref|auto-231>>

    <index+1|/:msg:dht:client:get|<pageref|auto-195>>

    <index+1|/:msg:dht:client:put|<pageref|auto-198>, <pageref|auto-205>>

    <index+1|/:msg:dht:client:result|<pageref|auto-200>, <pageref|auto-202>,
    <pageref|auto-207>>

    <index+1|netstruct|<pageref|auto-44>>

    <index+1|network size estimation|<pageref|auto-123>>

    <index+1|network structure|<pageref|auto-43>>

    <index+1|NSE server object|<pageref|auto-127>>

    <index+1|NSE|<pageref|auto-124>>

    <index+1|open-channel!|<pageref|auto-223>>

    <index+1|&overly-large-datum|<pageref|auto-158>>

    <index+1|&overly-large-paths|<pageref|auto-177>>

    <index+1|path element|<pageref|auto-172>>

    <index+1|port|<pageref|auto-219>>

    <index+1|properties|<pageref|auto-51>>

    <index+1|put!|<pageref|auto-185>>

    <index+1|put path|<pageref|auto-174>>

    <index+1|query?|<pageref|auto-167>>

    <index+1|query object|<pageref|auto-150>>

    <index+1|query-desired-replication-level|<pageref|auto-166>>

    <index+1|query-key|<pageref|auto-165>>

    <index+1|query-type|<pageref|auto-164>>

    <index+1|R5N|<pageref|auto-141>>

    <index+1|read%|<pageref|auto-54>>

    <index+1|read-value|<pageref|auto-27>>

    <index+1|report-error|<pageref|auto-111>>

    <index+1|search object|<pageref|auto-183>>

    <index+1|search result object|<pageref|auto-151>>

    <index+1|searching the DHT|<pageref|auto-180>>

    <index+1|search-result?|<pageref|auto-170>>

    <index+1|search-result-get-path|<pageref|auto-175>>

    <index+1|search-result-\<gtr\>datum|<pageref|auto-169>>

    <index+1|search-result-put-path|<pageref|auto-176>>

    <index+1|select|<pageref|auto-57>>

    <index+1|send-message!|<pageref|auto-91>>

    <index+1|server object|<pageref|auto-64>>

    <index+2|server object|CADET|<pageref|auto-214>>

    <index+2|server object|DHT|<pageref|auto-146>>

    <index+2|server object|NSE|<pageref|auto-128>>

    <index+1|service|<pageref|auto-62>>

    <index+1|services|<pageref|auto-68>>

    <index+1|set%!|<pageref|auto-55>>

    <index+1|set-value!|<pageref|auto-26>>

    <index+1|sizeof|<pageref|auto-56>>

    <index+1|start-get!|<pageref|auto-182>>

    <index+1|stop-get!|<pageref|auto-184>>

    <index+1|string-\<gtr\>data|<pageref|auto-248>>

    <index+1|string-\<gtr\>ecdsa-public-key|<pageref|auto-243>>

    <index+1|string-\<gtr\>eddsa-public-key|<pageref|auto-242>>

    <index+1|structure/packed|<pageref|auto-46>>

    <index+1|symbol-value|<pageref|auto-87>>

    <index+1|synopsis|<pageref|auto-49>>

    <index+1|terminal condition|<pageref|auto-118>>

    <index+1|tests|<pageref|auto-7>>

    <index+1|textual-error-reporting-port|<pageref|auto-112>>

    <index+1|tunnel|<pageref|auto-217>>

    <index+1|&undefined-key-error|<pageref|auto-28>>

    <index+1|update procedure|<pageref|auto-130>>

    <index+1|value-\<gtr\>index|<pageref|auto-89>>

    <index+1|variable assignment|<pageref|auto-39>>

    <index+1|verifier|<pageref|auto-82>>
  </the-index>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|project-flag|true>
    <associate|save-aux|false>
  </collection>
</initial>