;#!r6rs
;; This file is part of GNUnet.
;; Copyright (C) 2001-2013, 2018 GNUnet e.V.
;;
;; GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; C file: util/time.c
;; Author: Christian Grothoff
;; Partially adapted to Scheme by Maxime Devos
;; Synopsis: procedures for handling time and time arithmetic

(define-library (gnu gnunet util time)
  (export time-unit:zero time-unit:microsecond time-unit:millisecond
	  time-unit:second time-unit:minute time-unit:hour
	  standard-back-off)
  (import (only (rnrs base)
		assert integer? exact <= define begin min * max
		and exact? >= /))
  (begin
    (define time-unit:zero 0)
    (define time-unit:microsecond 1)
    (define time-unit:millisecond 1000)
    (define time-unit:second (* 1000 1000))
    (define time-unit:minute (* 60 time-unit:second))
    (define time-unit:hour (* 60 time-unit:minute))

    (define (exact-natural? x)
      (and (integer? x) (exact? x) (>= x 0)))

    ;; Treshold after which exponential backoff should not increase
    ;; (15 minutes).
    (define standard-exponential-backoff-treshold
      (/ time-unit:hour 4))

    (define (standard-back-off relative-time)
      "Perform our standard exponential back-off calculation, starting at
1 millisecond and then going by a factor of 2 up unto a maximum of 15 minutes.
The current backoff time is @var{relative-time}.  Initially it is zero."
      (assert (exact-natural? relative-time))
      (min standard-exponential-backoff-treshold
	   (* 2 (max time-unit:millisecond relative-time))))))
