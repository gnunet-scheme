;#!r6rs
;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright © 2021, 2022 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Brief: Interface to the network size estimation service.
;; C author: Nathan Evans
;; Scheme author: Maxime Devos
;;
;; The Scheme implementation is _not_ based on the C implementation,
;; though the Scheme API is structured after the C API and slightly
;; extended.

(define-library (gnu gnunet nse client)
  (export estimate?
	  estimate:logarithmic-number-peers
	  estimate:number-peers
	  estimate:standard-deviation
	  estimate:timestamp
	  (rename (server:nse? server?))
	  connect
	  disconnect!
	  estimate)
  (import (only (rnrs base)
		begin define quote lambda values expt = apply
		and >= let or nan?)
	  (only (rnrs records syntactic)
		define-record-type)
          (only (ice-9 atomic)
		make-atomic-box atomic-box-ref atomic-box-set!)
	  (only (ice-9 match)
		match)
          (only (fibers)
		spawn-fiber)
	  (only (gnu extractor enum)
		symbol-value value->index)
	  (only (guile)
		lambda* define*)
	  (only (gnu gnunet concurrency lost-and-found)
		losable-lost-and-found)
	  (only (gnu gnunet util struct)
		/:message-header)
	  (only (gnu gnunet utils bv-slice)
		make-slice/read-write slice-length)
          (only (gnu gnunet netstruct syntactic)
		read% set%! sizeof)
          (only (gnu gnunet mq handler)
		message-handler
		message-handlers)
          (only (gnu gnunet mq)
		send-message!)
          (gnu gnunet message protocols)
	  (only (gnu gnunet server)
		<server> make-disconnect!
		handle-control-message!
		<loop> spawn-server-loop run-loop loop:terminal-condition)
          (only (gnu gnunet nse struct)
		/:msg:nse:estimate))
  (begin
    (define-record-type (<estimate> %make-estimate estimate?)
      (fields (immutable logarithmic-number-peers
			 %estimate:logarithmic-number-peers)
	      (immutable standard-deviation %estimate:standard-deviation)
	      (immutable timestamp %estimate:timestamp))
      (sealed #t)
      (opaque #t))

    (define-record-type (<server:nse> %make-server server:nse?)
      (parent <server>) ; for automatic fibers disposal when the <server> is unreachable
      ;; Atomic box of <estimate>
      (fields (immutable estimate/box server-estimate/box))
      (protocol
       (lambda (%make)
	 (lambda ()
	   ((%make) (make-atomic-box #false))))))

    (define (estimate server)
      "Return the current estimate of the number of peers on the network,
in some unspecified format.  Alternatively, if no estimate is available
yet, return @code{#false}."
      (atomic-box-ref (server-estimate/box server)))

    (define (estimate:logarithmic-number-peers estimate)
      "Return the logarithm (base 2) of the number of peers on the network
in @var{estimate} as a positive flonum (possibly zero or infinite)."
      (%estimate:logarithmic-number-peers estimate))

    (define (estimate:standard-deviation estimate)
      "Return the standard deviation of the logarithmic estimate
of the number of peers of the last 64 rounds as a positive flonum
(possibly zero or infinite), or not-a-number."
      (%estimate:standard-deviation estimate))

    (define (estimate:number-peers estimate)
      "Return the estimate of the number of peers on the network in
@var{estimate} as a strictly-positive flonum, at least @code{1.0}
and possibly infinite."
      (expt 2.0 (estimate:logarithmic-number-peers estimate)))

    (define (estimate:timestamp estimate)
      "Return when the estimate @var{estimate} was made, as a standard GNUnet
timestamp."
      (%estimate:timestamp estimate))

    (define disconnect!
      (make-disconnect! 'network-size server:nse?))

    (define-record-type (<loop:nse> make-loop:nse loop:nse?)
      (parent <loop>)
      (fields (immutable updated loop:updated)
	      (immutable estimate/box loop:estimate/box))
      (protocol
       (lambda (%make)
	 (lambda* (#:key (updated values) estimate/box #:allow-other-keys
		   #:rest r)
	   ((apply %make r) updated estimate/box)))))

    ;; See 'connect'.  TODO: gc test fails
    (define* (handle-estimate! estimate-slice estimate/box updated)
      (define estimate
	(%make-estimate
	 (read% /:msg:nse:estimate '(size-estimate) estimate-slice)
	 (read% /:msg:nse:estimate '(std-deviation) estimate-slice)
	 (read% /:msg:nse:estimate '(timestamp) estimate-slice)))
      (atomic-box-set! estimate/box estimate)
      (updated estimate))

    (define (make-message-handlers loop)
      (message-handlers
       (message-handler
	(type (symbol-value message-type msg:nse:estimate))
	((interpose code) code)
	((well-formed? slice)
	 (and (= (slice-length slice)
		 (sizeof /:msg:nse:estimate '()))
	      ;; XXX: there is no test verifying these two expressions
	      ;; are present
	      (>= (read% /:msg:nse:estimate '(size-estimate) slice) 0)
	      ;; See <https://bugs.gnunet.org/view.php?id=7021#c18399> for
	      ;; situations in which the deviation can be infinite or NaN.
	      (let ((stddev (read% /:msg:nse:estimate '(std-deviation) slice)))
		(or (>= stddev 0) (nan? stddev)))))
	((handle! slice) (handle-estimate! slice (loop:estimate/box loop)
					   (loop:updated loop))))))

    (define (send-start! message-queue)
      ;; The service only starts sending estimates once
      ;; /:msg:nse:start is sent.
      (define s (make-slice/read-write (sizeof /:message-header '())))
      (set%! /:message-header '(size) s (sizeof /:message-header '()))
      (set%! /:message-header '(type) s
	     (value->index (symbol-value message-type msg:nse:start)))
      (send-message! message-queue s))

    (define (control-message-handler message continue continue* message-queue
				     loop)
      (define (k/reconnect!)
	(run-loop loop))
      (match message
        (('resend-old-operations!)
	 (send-start! message-queue)
	 (continue loop))
	(('lost . _)
	 ;; We lost ourselves, that means the server became unreachable.
	 ;; The presence of this line is tested by the "garbage collectable"
	 ;; test.
	 (continue* '(disconnect!) loop))
	(rest
	 (handle-control-message! message message-queue
				  (loop:terminal-condition loop) k/reconnect!))))

    (define* (connect config #:key updated connected disconnected spawn
		      #:rest r)
      "Connect to the NSE service in the background.

When connected, the thunk @var{connected} is called and estimates
will become available (but possibly not immediately).  When a new
estimate is available, the procedure @var{updated} is called.  This
procedure should accept the new estimate.   When disconnected, the
thunk @code{disconnected} and updates will be (temporarily) unavailable,
until connected again.  It is possible for @var{updated} to be called
shortly after calling @var{disconnected}.

The procedures @var{updated}, @var{connected} and @var{disconnected} are optional."
      (define server (%make-server))
      (apply spawn-server-loop server #:make-loop make-loop:nse
	     #:make-message-handlers make-message-handlers
	     #:control-message-handler control-message-handler
	     #:service-name "nse"
	     #:configuration config
	     #:estimate/box (server-estimate/box server) r))))
