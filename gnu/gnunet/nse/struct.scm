;#!r6rs
;; This file is part of scheme-GNUnet
;; Copyright (C) 2001-2011, 2021 GNUnet e.V.
;;
;; GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Author (GNUnet): Nathan Evans
;; File (GNUnet): nse/nse.h
;;
;; Brief: Common type definitions for the network size estimation
;; service and API.

(define-library (gnu gnunet nse struct (0 0))
  (export /:msg:nse:estimate
	  /:msg:nse:flood)
  (import (only (rnrs base) begin quote)
	  (only (gnu gnunet util struct)
		/:message-header /time-absolute)
	  (only (gnu gnunet crypto struct)
		/peer-identity /eddsa-signature /ecc-signature-purpose)
	  (only (gnu gnunet netstruct syntactic)
		define-type structure/packed)
	  (only (gnu gnunet netstruct procedural)
		u32/big u64/big ieee-double/big))
  (begin
    ;; XXX check for mistakes

    (define-type /:msg:nse:estimate
      (structure/packed
       (synopsis "Network size estimate sent from the service to clients")
       (documentation
	"Contains the current size estimate
(or 0 if none has been calculated) and the
standard deviation of known estimates.")
       (properties '((message-symbol msg:nse:estimate)
		     (c-type . GNUNET_NSE_ClientMessage)))
       (field (header /:message-header))
       (field (reserved u32/big))
       (field (timestamp /time-absolute)
	      (synopsis "Timestamp for when the estimate was made"))
       (field (size-estimate ieee-double/big))
       (field (std-deviation ieee-double/big))))

    (define-type /:msg:nse:flood
      (structure/packed
       (synopsis "Network size estimate reply")
       (documentation
	"Sent when \"this\" peer's timer has run out before receiving a
valid reply from another peer.")
       (properties '((message-symbol msg:nse:flood)
		     (c-type . GNUNET_NSE_FloodMessage)))
       (field (header /:message-header))
       (field (hop-count u32/big)
	      (synopsis
	       "Number of hops this message has taken so far"))
       (field (purpose /ecc-signature-purpose)
	      (synopsis "Purpose"))
       (field (timestamp /time-absolute)
	      (synopsis "The current timestamp value (which all
peers should agree on)"))
       (field (matching-bits u32/big)
	      ;; XXX add to (gnu gnunet util struct)
	      ;; XXX I don't understand the above XXX anymore
	      (synopsis
	       "Number of matching bits between the hash
of timestamp and the initiator's public key."))
       (field (origin /peer-identity)
	      (synopsis "Public key of the originator"))
       (field (proof-of-work u64/big)
	      (synopsis
	       "Proof of work, causing leading zeros when hashed with pkey."))
       (field (signature /eddsa-signature)
	      (synopsis "Signature (over range specified in purpose)."))))))
