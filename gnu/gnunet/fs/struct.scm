;; This file is part of Scheme-GNUnet.
;; Copyright © 2003--2012, 2022--2023 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; C source file: fs/fs.h
;; Network-related definitions for file-sharing
;; Author: Igor Wronsky, Christian Grothoff
;; Ported to Scheme-GNUnet by: Maxime Devos
(define-library (gnu gnunet fs struct)
  (export /content-hash-key
	  /:msg:fs:request-loc-signature
	  /:msg:fs:response-loc-signature
	  /:msg:fs:index-start!
	  /:msg:fs:index-list:entry
	  /:msg:fs:unindex!
	  /:msg:fs:start-search!
	  /:msg:fs:put!)
  (import (only (rnrs base) define begin * quote)
	  (only (gnu gnunet crypto struct)
		/eddsa-signature /peer-identity)
	  (only (gnu gnunet hashcode struct)
		/hashcode:512)
	  (only (gnu gnunet util struct)
		/:message-header /time-absolute)
	  (only (gnu gnunet netstruct procedural)
		u32/big u64/big make-netprimitive)
	  (only (gnu gnunet netstruct syntactic)
		define-type structure/packed))
  (begin
    ;; TODO: interaction with <content-hash-key>
    (define-type /content-hash-key
      (structure/packed
       (synopsis "Content hash key")
       (properties '((c-type . ContentHashKey)))
       (field (key /hashcode:512)
	      (synopsis "Hash of the original content, used for encryption"))
       (field (query /hashcode:512)
	      (synopsis "Hash of the encrypted content, used for querying"))))

    (define-type /:msg:fs:request-loc-signature
      (structure/packed
       (synopsis "Message sent by a FS client to request a LOC signature.")
       (properties '((message-symbol msg:fs:request-loc-signature)
		     (c-type . RequestLocSignatureMessage)))
       (field (header /:message-header))
       (field (purpose u32/big)
	      (synopsis "Requested signature purpose")
	      (documentation "For now, always GNUNET_SIGNATURE_PURPOSE_PEER_PLACEMENT"))
       (field (expiration-time /time-absolute)
	      (synopsis "Requested expiration time"))
       (field (content-hash-key /content-hash-key)
	      (synopsis "Information about the shared file (to be signed"))
       (field (file-length u64/big)
	      (synopsis "Size of the shared file (to be signed)"))))

    (define-type /:msg:fs:response-loc-signature
      (structure/packed
       (synopsis "Message sent by the service to the client with a LOC signature.")
       (properties '((message-symbol msg:fs:response-loc-signature)
		     (c-type . ResponseLocSignatureMessage)))
       (field (header /:message-header))
       (field (purpose u32/big)
	      (synopsis "Requested signature purpose")
	      (documentation "For now, always GNUNET_SIGNATURE_PURPOSE_PEER_PLACEMENT"))
       (field (expiration-time /time-absolute)
	      (synopsis "Expiration time of this signature.")
	      (documentation "Expiration time that was actually used (rounded!).
IIUC, not necessarily the time that was requested."))
       (field (signature /eddsa-signature)
	      (synopsis "The requested signature"))
       (field (peer /peer-identity)
	      (synopsis "Identity of the peer sharing the file.")
	      (documentation "On a typical setup, this is the identity of the
local peer on a typical setup, IIUC."))))

    (define-type /:msg:fs:index-start!
      (structure/packed
       (synopsis "Message to request indexing of a file.")
       (documentation "Message sent by a client to the service to request that
a file (on the file system) is indexed.  The service is supposed to check that
the specified file is available and has the same cryptographic hash as mentioned
in the message.  It should then respond with either a confirmation or a denial.

On operating systems where this works, it is considered acceptable if the
service only checks that the path, device and inode match (it can then be
assumed that the hash will also match without actually computing it; this
is an optimisation that should be safe given that the client is not our
advisary).

This message is followed by the file name of a file with the hash \"file-id\"
as seen by the client, then followed by an octet 0 (i.e., the file name is
zero-terminated).")
       (field (header /:message-header))
       (field (reserved u32/big)
	      (synopsis "For alignment."))
       (field (device u64/big)
	      (synopsis "ID of device containing the file")
	      ;; TODO: why write 64-bit and not 32-bit?
	      ;; Also, verify endianness against the implementations.
	      (documentation "ID of device containing the file, as seen by the
client.  This device ID is obtained using a call like \"statvfs\" (and
converting the \"f_fsid\" field to a 32-bit big-endian number).  Use 0 if
the operating system does not support this, in which case the service must
do a full hash recomputation."))
       (field (inode u64/big) ; TODO: check endianness
	      (synopsis "Inode of the file, on the device.")
	      (documentation "Inode of the file on the given device, as seen
by the client (\"st_ino\" field from \"struct stat\").  Use 0 if the operating
system does not support this, in which case the service must do a full hash
recomputation."))
       (field (file-id /hashcode:512)
	      (synopsis "Hash of the file that we would like to index."))))

    (define-type /:msg:fs:index-list:entry
      (structure/packed
       (synopsis "Entry in the list of all indexed files")
       (properties '((message-symbol msg:fs:index-list:entry)
		     (c-type . IndexInfoMessage)))
       (documentation "Message sent by FS service in response to a request
for all indexed files (i.e., a msg:fs:index-list:get).  Later, more of the
same type of messages are sent or a msg:fs:index-list:end.

This is folowed by a 0-terminated file name of a file with the hash
@var{file-id} as seen by the client.")
       (field (header /:message-header))
       (field (reserved u32/big)
	      (synopsis "Always zero."))
       (field (file-id /hashcode:512)
	      (synopsis "Hash of the indexed file."))))

    (define-type /:msg:fs:unindex!
      (structure/packed
       (synopsis "Message to the service to indicate a file wile be unindexed.")
       (properties '((message-symbol msg:fs:unindex!)
		     (c-type . UnindexMessage)))
       (documentation "Message sent from a GNUnet client (IIUC, something about
'unindexing activity') to the file-sharing service to indicate that a file will
be unindexed.  The service is supposed to remove the file from the list of
unindexed files and respond with a confirmation message (even if the file was
already not on the list).")
       (field (header /:message-header))
       (field (reserved u32/big)
	      (synopsis "Always zero."))
       (field (file-id /hashcode:512)
	      (synopsis "Hash of the file that we will unindex."))))

    (define-type /:msg:fs:start-search!
      (structure/packed
       (synopsis "Message to the service to start a (keyword) search.")
       (documentation "This is followed by the hash codes of already-known
results (which should hence be excluded from what the service returns);
naturally, this only applies to queries that can have multiple results
(UBLOCKS). ")
       (properties '((message-symbol msg:fs:start-search!)
		     (c-type . SearchMessage)))
       (field (header /:message-header))
       ;; TODO: better support bitmasks in (gnu gnunet netstruct ...)
       ;; TODO: SEARCH_MESSAGE_OPTION_???
       (field (options u32/big)
	      (synopsis "Bitmask with search options")
	      (documentation "Zero for no options, one for loopback-only,
two for 'to be continued' (with a second search message for the same
type/query/target and additional already-known results following this one).
See @code{SEARCH_MESSAGE_OPTION_...}.  Other bits are currently undefined."))
       (field (type u32/big)
	      (synopsis "Type of the content that we are looking for.
TODO: reference to enumeration."))
       (field (anonymity-level u32/big)
	      ;; TODO: bounds, reference to interpretation.
	      (synopsis "Desired anonymity level"))
       (field (target /peer-identity)
	      (synopsis "Identity of peer known to have a response, if any.")
	      (documentation "If the request is for a DBLOCK or IBLOCK, this is
the identity of the peer that is known to have a response.  Set to all-zeros if
such a target is not known (note that even if @emph{our} anonymity level is >0,
we may happen to know the responder's identity; nevertheless, we should
probably not use it for a DHT-lookup or simiar blunt actions in order to avoid
exposing ourselves).

Otherwise, @var{target} must be all zeros."))
       (field (query /hashcode:512)
	      (synopsis "Hash of the public key for UBLOCKs: hash of the
CHK-encoded block for DBLOCKS and IBLOCKS."))))

    ;; TODO: client<->service or peer<->peer?
    (define-type /:msg:fs:put!
      (structure/packed
       (synopsis "Response from file-sharing service with a response for a
previous FS search.")
       (documentation "This is followed by the actual encrypted content.")
       (properties '((message-symbol msg:fs:put!) ; TODO: check
		     (c-type . PutMessage)))
       (field (header /:message-header))
       (field (type u32/big)
	      ;; TODO: reference to block types
	      (synopsis "Type of the block.  Should never be zero"))
       (field (expiration /time-absolute)
	      (synopsis "When does this result expire?"))))

    (define-type /:msg:fs:client:put!
      (structure/packed
       (synopsis "Response from file-sharing service with a response for a
previous FS search")
       (documentation
        "Response from FS service with a result for a previous FS search.
Note that queries for DBLOCKS and IBLOCKS that have received a
single response are considered done.  This message is transmitted
between the service and a client.")
       (properties '((c-type . ClientPutMessage)
		     ;; This being the same as for /:msg:fs:client:put!
		     ;; is correct.
		     (message-symbol msg:fs:put!)))
       (field (header /:message-header))
       (field (type u32/big)
	      ;; TODO: reference to list of block types.
	      (synopsis "Type of the block.  Should never be zero."))
       (field (expiration /time-absolute)
	      (synopsis "When does this result expire?"))
       (field (last-transmission /time-absolute)
	      ;; TODO: Define FOREVER
	      (synopsis "When was the last time we have tried to download this
block?  FOREVER if unknown/not relevant."))
       (field (number-transmissions u32/big)
	      (synopsis "How often did we transmit this query before getting
an answer (estimate).")) ; TODO check begin at 0 or 1, when
       (field (respect-offered u32/big)
	      (synopsis "How much respect did we offer (in total) before
getting an answer (estimate)."))))))
