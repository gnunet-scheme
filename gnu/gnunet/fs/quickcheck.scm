;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright © 2022 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

(define-library (gnu gnunet fs quickcheck)
  (export $content-hash-key $chk-uri)
  (import (only (rnrs base) begin define)
	  (only (quickcheck arbitrary) $record $natural)
	  (only (gnu gnunet fs uri)
		make-content-hash-key content-hash-key-key
		content-hash-key-query
		make-chk-uri chk-uri-file-length chk-uri-chk)
	  (only (gnu gnunet hashcode quickcheck) $hashcode:512))
  (begin
    (define $content-hash-key
      ($record make-content-hash-key
	       (content-hash-key-key $hashcode:512)
	       (content-hash-key-query $hashcode:512)))

    ;; Can produce some impossible chk URIs, e.g. multiple
    ;; chk URIs for length=0.
    (define $chk-uri
      ($record make-chk-uri
	       (chk-uri-file-length $natural) ; TODO: bounds
	       (chk-uri-chk $content-hash-key)))))
