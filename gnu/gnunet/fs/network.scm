;#!r6rs
;; This file is part of Scheme-GNUnet
;; Copyright © 2022, 2023 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; TODO: untested
(define-library (gnu gnunet fs network)
  (export construct-request-loc-signature analyse-request-loc-signature
	  construct-response-loc-signature analyse-response-loc-signature)
  (import (only (rnrs base) define values)
	  (only (guile) begin define*)
	  (only (gnu extractor enum) value->index symbol-value)
	  (only (gnu gnunet message protocols) message-type)
	  (only (gnu gnunet fs struct)
		/content-hash-key /:msg:fs:request-loc-signature
		/:msg:fs:response-loc-signature)
	  (only (gnu gnunet fs uri)
		content-hash-key-key ;; TODO rename
		content-hash-key-query
		make-content-hash-key/share)
	  (only (gnu gnunet hashcode)
		hashcode:512->slice)
	  (only (gnu gnunet netstruct syntactic)
		r% s% define-analyser construct %sizeof
		=>! =>slice!))
  (begin
    ;; GNUNET_SIGNATURE_PURPOSE_PEER_PLACEMENT,
    ;; (see gnunet-signatures/gnunet_signatures.rst)
    (define %purpose-peer-placement 5)

    (define* (construct-request-loc-signature file-length*
					      content-hash-key
					      expiration-time
					      #:key
					      (purpose %purpose-peer-placement))
      "Create a new @code{/:msg:fs:request-loc-signature} message for a file of
length @var{file-length} with @var{content-hash-key} as content hash key,
expiring at @var{expiration-time} (TODO type), for @var{purpose}
(currently always %purpose-peer-placement).  TODO bounds."
      (construct
       /:msg:fs:request-loc-signature
       (=>! (header size) (%sizeof))
       (=>! (header type)
	    (value->index
	     (symbol-value message-type msg:fs:request-loc-signature)))
       (=>! (purpose) purpose)
       (=>! (expiration-time) expiration-time)
       (=>slice! (content-hash-key key)
		 (hashcode:512->slice (content-hash-key-key content-hash-key)))
       (=>slice!
	(content-hash-key query)
	(hashcode:512->slice (content-hash-key-query content-hash-key)))
       (=>! (file-length) file-length*)))

    (define-analyser analyse-request-loc-signature
      /:msg:fs:request-loc-signature
      "Return the file length, content hash key, expiration time (TODO type)
and signature purpose corresponding to the @code{/:msg:fs:request-loc-signature}
message @var{message}."
      (values (r% file-length)
	      (make-content-hash-key/share (s% content-hash-key))
	      (r% expiration-time)
	      (r% purpose)))

    (define*
      (construct-response-loc-signature expiration-time signature peer
					#:key (purpose %purpose-peer-placement))
      "Create a new @code{/:msg:fs:response-loc-signature} message, for an EdDSA signature
@var{signature} (as a readable @code{/eddsa-signature} bytevector slice) by @var{peer}
(as a readable @code{/peer-identity} bytevector slice), expiring at @var{expiration-time}
(TODO type)."
      (construct
       /:msg:fs:response-loc-signature
       (=>! (header size) (%sizeof))
       (=>! (header type)
	    (value->index
	     (symbol-value message-type msg:fs:response-loc-signature)))
       (=>! (purpose) purpose)
       (=>! (expiration-time) expiration-time)
       (=>slice! (signature) signature)
       (=>slice! (peer) peer)))

    (define-analyser analyse-response-loc-signature
      /:msg:fs:response-loc-signature
      "Return the expiration time (TODO type), signature (as a slice of the message),
peer identity (as a slice of the message) and the signature purpose corresponding to
the @code{/:msg:fs:response-loc-signature} message."
      (values (r% expiration-time)
	      (s% signature)
	      (s% peer)
	      (r% purpose)))))
