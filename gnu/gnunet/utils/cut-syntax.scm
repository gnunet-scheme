;#!r6rs
;; This file is part of Scheme-GNUnet
;; Copyright © 2022 GNUnet e.V.
;;
;; Scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; Scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; TODO: eliminate (gnu gnunet netstruct syntactic), use a compiler pass instead
;; for inlining, then ‘cut-syntax’ can be deprecated in favour of a ‘cut’.
(define-library (gnu gnunet utils cut-syntax)
  (export cut-syntax)
  (import (only (rnrs base) ... begin define-syntax syntax-rules))
  (begin
    (define-syntax substitute
      (syntax-rules (<>)
	((_ (substituted ...) (<> . foos) (bar . bars))
	 (substitute (substituted ... bar) foos bars))
	((_ (substituted ...) (foo . foos) bars)
	 (substitute (substituted ... foo) foos bars))
	((_ (substituted ...) () ())
	 (substituted ...))))

    (define-syntax cut-syntax
      (syntax-rules ()
	((_ . foos)
	 (syntax-rules ()
	   ((_ . bars) (substitute () foos bars))))))))
