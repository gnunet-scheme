;#!r6rs
;; This file is part of GNUnet.
;; Copyright (C) 2010, 2021 GNUnet e.V.
;;
;; GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Author: Christian Grothoff
;; Adjusted for Scheme by Maxime Devos
(define-library (gnu gnunet block)
  (export block-type block-type?)
  (import (only (rnrs base)
		begin)
	  (only (gnu extractor enum)
		define-enumeration value))
  (begin
    ;; Blocks in the datastore and the datacache must have a unique type.
    ;; Corresponds to  GNUNET_BLOCK_Type in the C implementation.
    (define-enumeration (block-type block-type?)
      (#:documentation "Types of blocks in the datastore and the datacache.")
      (#:max (- (expt 2 32) 1))
      (#:known
       (value
	(symbol block:any)
	(index 0)
	(documentation "Any type of block, used as a wildcard when searching.
Should never be attached to a specific block."))
       (value
	(symbol block:fs:data)
	(index 1)
	(documentation "Data block (leaf) in the CHK tree"))
       (value
	(symbol block:fs:inner)
	(index 2)
	(documentation "Data block (leaf) in the CHK tree"))
       (value
	(symbol block:fs:k)
	(index 3)
	(documentation "Legacy type, no longer in use"))
       (value
	(symbol block:fs:s)
	(index 4)
	(documentation "Legacy type, no longer in use"))
       (value
	(symbol block:fs:n)
	(index 5)
	(documentation "Legacy type, no longer in use"))
       (value
	(symbol block:fs:on-demand)
	(index 6)
	(documentation "Type of a block representing a block to be encoded \
on demand from disk.\nShould never appear on the network directly."))
       (value
	(symbol block:dht:hello)
	(index 7)
	(documentation "Type of a block that contains a HELLO for a peer (for \
 DHT and CADET find-peer operations)"))
       (value
	(symbol block:test)
	(index 8)
	(documentation "Block for testing"))
       (value
	(symbol block:fs:universal)
	(index 9)
	(documentation "Type of a block representing any type of search result \
(universal).  Implemented in the context of #2564, replaces block:fs:s, \
block:fs:k and block:fs:n"))
       (value
	(symbol block:dns)
	(index 10)
	(documentation "Block for storing DNS exit service advertisements"))
       (value
	(symbol block:gns:name-record)
	(index 11)
	(documentation "Block for storing record data"))
       (value
	(symbol block:revocation)
	(index 12)
	(documentation "Block type for a revocation message by which a key is \
revoked"))
       (value
	(symbol block:filler:13)
	(index 13))
       (value
	(symbol block:filler:14)
	(index 14))
       (value
	(symbol block:filler:15)
	(index 15))
       (value
	(symbol block:filler:16)
	(index 16))
       (value
	(symbol block:filler:17)
	(index 17))
       (value
	(symbol block:filler:18)
	(index 18))
       (value
	(symbol block:filler:19)
	(index 19))
       (value
	(symbol block:filler:20)
	(index 20))
       (value
	(symbol block:filler:21)
	(index 21))
       (value
	(symbol block:regex)
	(index 22)
	(documentation "Block to store a cadet regex state"))
       (value
	(symbol block:regex:accept)
	(index 23)
	(documentation "Block to store a cadet regex accepting state"))
       (value
	;; GNUNET_BLOCK_TYPE_SET_TEST, GNUNET_BLOCK_TYPE_SETI_TEST,
	;; GNUNET_BLOCK_TYPE_SETU_TEST
	(symbol block:filler:24)
	(index 24)
	(documentation
	 "Block for testing set/consensus.  If first byte of the block \
is non-zero, the block is considered invalid."))
       (value
	(symbol block:consensus-element)
	(index 25)
	(documentation "Block type for consensus elements.
Contains either special marker elements or a nested block."))))))
