;#!r6rs
;; This file is part of GNUnet
;; Copyright © 2021, 2022 GNUnet e.V.
;;
;; GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later

;; Small wrapper around guile-gcrypt and other related procedures.
(define-library (gnu gnunet crypto)
  (export hash/sha512 hash/sha512!
	  &invalid-public-key-encoding
	  invalid-public-key-encoding?
	  string->eddsa-public-key
	  string->ecdsa-public-key)
  (import (only (gcrypt hash)
		hash-algorithm open-hash-port sha512)
	  (gnu gnunet utils bv-slice)
	  (only (gnu gnunet crypto struct)
		/eddsa-public-key /ecdsa-public-key)
	  (only (gnu gnunet data-string)
		string->data bogus-crockford-base32hex?)
	  (only (gnu gnunet netstruct syntactic)
		sizeof)
	  (only (gnu gnunet utils hat-let)
		let^)
	  (only (srfi srfi-8)
		receive)
	  (only (guile)
		%make-void-port close-port ceiling-quotient)
	  (only (ice-9 binary-ports)
		put-bytevector)
	  (only (rnrs base)
		begin lambda define quote * not = string-length or)
	  (only (rnrs conditions)
		define-condition-type &violation)
	  (only (rnrs exceptions)
		raise guard))
  (begin
    ;; TODO: Extend bytevector-hash with offset + length.
    (define (hash-slice/bytevector algorithm slice)
      "Hash the data in the readable bytevector slice @var{slice} and
return a bytevector with the resulting hash."
      (define slice/read (slice/read-only slice))
      (receive (port get-hash) (open-hash-port algorithm)
	(put-bytevector port
			(slice-bv slice/read)
			(slice-offset slice/read)
			(slice-length slice/read))
	(close-port port)
	(get-hash)))

    (define (hash-slice! algorithm slice to)
      "Hash the data in the readable bytevector slice @var{slice} and write the
hash to the bytevector slice @var{to}."
      (slice-copy! (bv-slice/read-only (hash-slice/bytevector algorithm slice))
		   to))

    (define (hash-slice algorithm slice)
      "Hash the data in the readable bytevector slice @var{slice} and return a
fresh readable bytevector slice with the hash."
      (bv-slice/read-only (hash-slice/bytevector algorithm slice)))

    (define (hasher! algorithm)
      (lambda (slice to)
	(hash-slice! algorithm slice to)))

    (define (hasher algorithm)
      (lambda (slice)
	(hash-slice algorithm slice)))

    ;; (hash/sha512! data-to-hash-slice destination-slice) --> (nothing)
    (define hash/sha512! (hasher! (hash-algorithm sha512)))
    (define hash/sha512 (hasher (hash-algorithm sha512)))

    
    ;; TODO: &who, how to decide between supertypes, maybe some
    ;; more specific subtypes and some fields
    (define-condition-type &invalid-public-key-encoding &violation
      make-invalid-public-key-encoding invalid-public-key-encoding?)

    (define (raise-invalid-public-key-encoding)
      (raise (make-invalid-public-key-encoding)))

    ;; See next two procedures.
    (define (string->something-public-key key-length/bytes string)
      (let^ ((! key-length/bits (* 8 key-length/bytes))
	     (! key-length/characters (ceiling-quotient key-length/bits 5))
	     (? (not (= key-length/characters (string-length string)))
		(raise-invalid-public-key-encoding)))
	    (guard (c ((bogus-crockford-base32hex? c)
		       ;; TODO: maybe add c to &irritants
		       (raise-invalid-public-key-encoding)))
		   (string->data string key-length/bytes))))

    ;; TODO: find some test cases
    (define (string->eddsa-public-key string)
      "Decode the Crockford Base32-encoded EdDSA public key @var{string} from a string
into a fresh bytevector.  In case of a decoding error (unexpected characters
or wrong length), an @code{&invalid-public-key-encoding} is raised.

This corresponds to @code{GNUNET_CRYPTO_ecdsa_public_key_from_string} in the C API."
      (string->something-public-key (sizeof /eddsa-public-key '()) string))

    (define (string->ecdsa-public-key string)
      "Decode the Crockford Base32-encoded ECDSA public key @var{string} from a string
into a fresh bytevector.  In case of a decoding error (unexpected characters
or wrong length), an @code{&invalid-public-key-encoding} is raised.

This corresponds to @code{GNUNET_CRYPTO_ecdsa_public_key_from_string} in the C API."
      (string->something-public-key (sizeof /ecdsa-public-key '()) string))))
