;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright (C) 2021 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(import (gnu gnunet mq error-reporting)
	(srfi srfi-43)
	(only (rnrs base) assert))

(test-begin "error-reporting")

(define %errors
  '((connection:connected)
    (connection:interrupted)
    (input:regular-end-of-file)
    (input:premature-end-of-file)
    (input:overly-small 1 3)
    (input:overly-small #f 3)
    (logic:no-handler 5)
    (logic:ill-formed 6)
    (some unexpected error !)))

(test-assert "report-error doesn't fail"
  (begin
    (for-each (lambda (x)
		(apply report-error x))
	      %errors)
    #t))

(test-assert "report-error-textually ends with a newline"
  (for-each
   (lambda (x)
     (pk 'x x)
     (assert (string-suffix? "\n"
			     (call-with-output-string
			       (lambda (port)
				 (apply report-error-textually port x)))))
     #t)
   %errors))

(test-equal "report-error output can be redirected"
  (call-with-output-string
    (lambda (port)
      (parameterize ((textual-error-reporting-port port))
	(apply report-error '(logic:no-handler 1)))))
  (call-with-output-string
    (lambda (port)
      (parameterize ((current-error-port port))
	(apply report-error '(logic:no-handler 1))))))

(test-end "error-reporting")
