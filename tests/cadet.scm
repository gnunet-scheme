;; This file is part of scheme-GNUnet, a partial Scheme port of GNUnet.
;; Copyright © 2022 GNUnet e.V.
;;
;; scheme-GNUnet is free software: you can redistribute it and/or modify it
;; under the terms of the GNU Affero General Public License as published
;; by the Free Software Foundation, either version 3 of the License,
;; or (at your option) any later version.
;;
;; scheme-GNUnet is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Affero General Public License for more details.
;;
;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;; SPDX-License-Identifier: AGPL-3.0-or-later
(define-module (test-distributed-hash-table))
(import (gnu gnunet cadet client)
	(gnu gnunet cadet network)
	(only (gnu gnunet cadet struct)
	      %minimum-local-channel-id
	      /:msg:cadet:local:channel:create)
	(gnu gnunet utils bv-slice)
	(gnu gnunet utils bv-slice-quickcheck)
	(gnu gnunet utils hat-let)
	(gnu gnunet netstruct syntactic)
	(gnu gnunet crypto struct)
	(gnu gnunet hashcode struct)
	(gnu gnunet mq)
	(only (gnu gnunet mq envelope)
	      envelope-peek-cancelled?
	      envelope-peek-irrevocably-sent?)
	(gnu gnunet message protocols)
	(gnu gnunet message protocols)
	(gnu gnunet mq handler)
	(gnu extractor enum)
	(only (gnu gnunet mq-impl stream)
	      port->message-queue)
	(rnrs bytevectors)
	(only (fibers scheduler)
	      yield-current-task)
	(ice-9 match)
	(srfi srfi-8)
	(srfi srfi-64)
	(tests utils)
	(quickcheck)
	(quickcheck property)
	(quickcheck generator)
	(quickcheck arbitrary)
	(rnrs base)
	(only (fibers conditions)
	      make-condition
	      wait
	      signal-condition!)
	(only (fibers channels)
	      make-channel
	      get-message
	      put-message))

(test-begin "CADET")
(test-assert "(CADET) close, not connected --> all fibers stop, no callbacks called"
  (close-not-connected-no-callbacks "cadet" connect disconnect!))

(test-assert "(CADET) garbage collectable"
 (garbage-collectable "cadet" connect))

(test-assert "(CADET) notify disconnected after end-of-file, after 'connected'"
	     (disconnect-after-eof-after-connected "cadet" connect))

(test-assert "(CADET) reconnects"
	     (reconnects "cadet" connect))

(define %peer-identity
  (bv-slice/read-write (u8-list->bytevector (iota (sizeof /peer-identity '())))))
(define %port
  (bv-slice/read-write
   (u8-list->bytevector (map (lambda (x) (- 255 x))
			     (iota (sizeof /hashcode:512 '()))))))
(test-assert "cadet-address?"
  (and (cadet-address? (make-cadet-address %peer-identity %port))
       (not (cadet-address? 'foobar))))

(test-equal "cadet-address, deconstruct"
  '(#true #true)
  (let ((cadet (make-cadet-address %peer-identity %port)))
    ;; TODO: extend 'bytevector=?' to accept ranges, then define
    ;; 'slice=?'.
    (list (equal? (cadet-address-peer cadet) (slice/read-only %peer-identity))
	  (equal? (cadet-address-port cadet) (slice/read-only %port)))))

(test-error "cadet-address, wrong peer identity size (1)" #f
	    (make-cadet-address (make-slice/read-write 0) %port))
(test-error "cadet-address, wrong peer identity size (2)" #f
	    (make-cadet-address
	     (make-slice/read-write (- (sizeof /peer-identity '()) 1)) %port))
(test-error "cadet-address, wrong peer identity size (3)" #f
	    (make-cadet-address
	     (make-slice/read-write (+ (sizeof /peer-identity '()) 1)) %port))

(test-error "cadet-address, wrong port size (1)" #f
	    (make-cadet-address %peer-identity (make-slice/read-write 0)))
(test-error "cadet-address, wrong port size (2)" #f
	    (make-cadet-address
	     %peer-identity
	     (make-slice/read-write (- (sizeof /hashcode:512 '()) 1))))
(test-error "cadet-address, wrong port size (3)" #f
	    (make-cadet-address
	     %peer-identity
	     (make-slice/read-write (+ (sizeof /hashcode:512 '()) 1))))

(test-assert "cadet-address, read-only port"
  (let ((slice (cadet-address-port (make-cadet-address %peer-identity %port))))
    (and (slice-readable? slice) (not (slice-writable? slice)))))
(test-assert "cadet-address, read-only peer"
  (let ((slice (cadet-address-peer (make-cadet-address %peer-identity %port))))
    (and (slice-readable? slice) (not (slice-writable? slice)))))

(test-assert "cadet-address, independent slices"
  (let ((struct (make-cadet-address %peer-identity %port)))
    (and (slice-independent? %peer-identity (cadet-address-peer struct))
	 (slice-independent? %port (cadet-address-port struct)))))

(test-equal "cadet-address, equal?"
  (make-cadet-address %peer-identity %port)
  (make-cadet-address (slice-copy/read-only %peer-identity)
		      (slice-copy/read-only %port)))

;; TODO: integrate in guile-quickcheck, (tests utils)
(define ($integer-in-range lower upper)
  (arbitrary
   (gen (choose-integer lower upper))
   (xform #false)))
(define ($arbitrary-lift f . a)
  (arbitrary
   (gen (apply generator-lift f (map arbitrary-gen a)))
   (xform #false))) ; TODO

(define $channel-number ($integer-in-range 0 (- (expt 2 32) 1)))
(define $peer ($sized-bytevector-slice/read-only (sizeof /peer-identity '())))
(define $port ($sized-bytevector-slice/read-only (sizeof /hashcode:512 '())))
(define $options ($integer-in-range 0 (- (expt 2 32) 1)))
(define $cadet-address ($arbitrary-lift make-cadet-address $peer $port))
(define $priority-preference ($integer-in-range 0 (- (expt 2 32) 1)))
;; Actual sizes can be a lot larger
(define $cadet-data ($sized-bytevector-slice/read-only 500))

(define (normalise list)
  (map (match-lambda
	 ((? slice? s) (slice-copy/read-only s))
	 (foo foo))
       list))

(test-roundtrip "analyse + construct round-trips (local-channel-create)"
		analyse-local-channel-create construct-local-channel-create
		normalise
		(cadet-address $cadet-address)
		(channel-number $channel-number)
		(options $options))
(test-roundtrip "analyse + construct round-trips (local-channel-destroy)"
		analyse-local-channel-destroy construct-local-channel-destroy
		normalise
		(channel-number $channel-number))
(test-roundtrip "analyse + construct round-trips (local-data)"
		analyse-local-data construct-local-data
		normalise
		(channel-number $channel-number)
		(priority-preference $priority-preference)
		(data $cadet-data))
(test-roundtrip "analyse + construct round-tripes (local-acknowledgement)"
		analyse-local-acknowledgement construct-local-acknowledgement
		normalise
		(channel-number $channel-number))

;; header information will be tested elsewhere (TODO)



;;;
;;; Test client ↔ server communication
;;;

(define (no-operation . _)
  (values))

;; Some arbitrary (*) message and address.
;; (*): TODO: size limits
(define message (bv-slice/read-write #vu8(0 0 0 0)))
(define address0 (make-cadet-address %peer-identity %port))

(define (no-error-handler . _)
  (pk 'a _)
  (error 'no-error-handler "oops"))

(define no-operation-message-handler/channel-create
  (message-handler
   (type (symbol-value message-type msg:cadet:local:channel:create))
   ;; TODO: make these optional
   ((interpose exp) exp)
   ((well-formed? s) #true) ; not tested here.
   ((handle! s) (values)))) ; not tested here.

(define no-operation-message-handler/local-data
  (message-handler
   (type (symbol-value message-type msg:cadet:local:data))
   ;; TODO: make these optional
   ((interpose exp) exp)
   ((well-formed? s) #true) ; not tested here.
   ((handle! s) (values)))) ; not tested here.

(test-equal
 "data is not sent before an acknowledgement"
 '(#false #false)
 (call-with-services/fibers
  `(("cadet" . ,(lambda (port spawn-fiber)
		  (define message-queue
		    (port->message-queue
		     port
		     (message-handlers
		      no-operation-message-handler/channel-create)
		     no-error-handler #:spawn spawn-fiber))
		  (values))))
  (lambda (config spawn-fiber)
    (define server (connect config #:spawn spawn-fiber))
    (define channel
      (open-channel! server address0 (message-handlers)))
    (define message-queue
      (channel-message-queue channel))
    ;; Try to send something, the actual sending should be delayed indefinitely
    ;; as the simulated server won't send an acknowledgement.  If it sent anyway,
    ;; then the envelope is marked as irrevocably sent and the error handler is
    ;; called because of a missing error handler for msg:cadet:local:data.
    (define envelope (send-message! message-queue message))
    ;; Give the other fibers a chance to mess up.
    (let loop ((n 100))
      (when (> n 0)
	(yield-current-task)
	(loop (- n 1))))
    ;; Might as well test it hasn't been cancelled while we're at it.
    (list (envelope-peek-cancelled? envelope)
	  (envelope-peek-irrevocably-sent? envelope)))
  ;; These two options make yield-current-task more reliable
  #:hz 0
  #:parallelism 1))

(define acknowledgement
  ;; XXX: the implementation doesn't have to start at that number, it could
  ;; start later, maybe avoid this implementation detail in the tests.
  (construct-local-acknowledgement %minimum-local-channel-id))

(test-assert
 "data is properly sent in response to acknowledgements, in-order" ; TODO: is the in-order a requirement?
 (quickcheck
  ;; In each round, a number of messages are sent.
  ;; At the same time (asynchronuously), some acknowledgements are sent.
  ;;
  ;; It is verified that, when there is a sufficient amount of acknowledgements,
  ;; the messages are all sent to the service, that they aren't sent too early
  ;; and that they are sent in-order.
  (property ((messages+acknowledgements
	      ($list
	       ($choose
		;; 'error': we aren't generating functions, so it will
		;; be unused.
		(error ($arbitrary-lift vector
					;; Number of messages to send
					$natural
					;; Number of acknowledgements to send
					$natural))
		;; 'synchronize': Verify that the messages are going to the
		;; server.  This is not done after every round, because that
		;; would reduce the amount of concurrency and hence the scope
		;; of the test.
		(error ($const 'synchronize))))))
    (pk 'iter) ; indicate it's not hanging
    (let ((server-channel (make-channel))
	  ;; No atomic boxes are required here even though they are accessed and mutated
	  ;; from multiple fibers, because of #:parallelism 0 and #:hz 0.
	  (n-received 0)
	  (n-added 0) ; how many messages have been added to the queue so far
	  (n-sent 0) ; how many of those messages have been irrevocably sent
	  (total-acknowledgements 0))
      (call-with-services/fibers
       `(("cadet" .
	  ,(lambda (port spawn-fiber)
	     (define message-handler/local-data
	       (message-handler ; TODO: simple-message-handler
		(type (symbol-value message-type msg:cadet:local:data))
		((interpose exp) exp)
		((well-formed? s) #true)
		((handle! message)
		 (set! n-received (+ 1 n-received))
		 (assert (<= n-received n-sent))
		 (assert (<= n-received n-added))
		 (assert (<= n-received total-acknowledgements))
		 (values))))
	     (define message-queue
	       (port->message-queue port
				    (message-handlers
				     no-operation-message-handler/channel-create
				     message-handler/local-data)
				    no-error-handler
				    #:spawn spawn-fiber))
	     (let loop ()
	       (match (get-message server-channel)
	         ((? integer? n)
		  ;; Send a few acknowledgements.
		  (let loop2 ((n n))
		    (cond ((<= n 0)
			   (loop))
			  (#true
			   (send-message! message-queue acknowledgement)
			   (loop2 (- n 1))))))
		 ('stop (values)))))))
       (lambda (config spawn-fiber)
	 (define server (connect config #:spawn spawn-fiber))
	 (define channel
	   (open-channel! server address0 (message-handlers)))
	 (define message-queue
	   (channel-message-queue channel))
	 (define (make-notify-sent! i)
	   (lambda ()
	     ;; Verify that messages were sent in-order,
	     ;; by verifying that all the previous envelopes
	     ;; have been sent.
	     (assert (= n-sent i))
	     ;; TODO: this assumes messages aren't sent in parallel, maybe document that
	     ;; messages are sent sequentially.
	     (set! n-sent (+ n-sent 1))
	     ;; an additional check.
	     (assert (<= n-sent n-added))
	     ;; Verify that the number of acknowledgements is respected.
	     (assert (<= n-sent total-acknowledgements))
	     (values)))
	 (let loop ((remaining messages+acknowledgements))
	   (match remaining
	     (('synchronize . remaining)
	      ;; Check that all the messages that could be sent are sent
	      ;; and received (no corking was requested, and the loop simulates
	      ;; passage of some time).
	      (let loop ((old-to-be-sent +inf.0)
			 (old-to-be-received +inf.0))
		(define new-to-be-sent
		  (- (min total-acknowledgements n-added) n-sent))
		(define new-to-be-received
		  (- (min total-acknowledgements n-added) n-received))
		(assert (<= 0 new-to-be-sent))
		(assert (<= 0 new-to-be-received))
		(assert (or (< new-to-be-sent old-to-be-sent) ; bail out if no progress is made
			    (< new-to-be-received old-to-be-received)))
		(when (or (< 0 new-to-be-sent)
			  (< 0 new-to-be-received))
		  ;; Give the various fibers a chance to process the messages. The allowed
		  ;; amount of context switched is proportional to the number of messages
		  ;; that still need to be sent. The number 16 is an over-approximation,
		  ;; the exact value doesn't matter to this test.
		  (let loop* ((n (* 16 (+ 1 (+ new-to-be-sent new-to-be-received)))))
		    (when (> n 0)
		      (yield-current-task)
		      (loop* (- n 1))))
		  (loop new-to-be-sent new-to-be-received)))
	      (loop remaining))
	     ((#(n-new-messages n-new-acknowledgements) . remaining)
	      (put-message server-channel n-new-acknowledgements)
	      (set! total-acknowledgements
		    (+ n-new-acknowledgements total-acknowledgements))
	      (let loop2 ((k 0))
		(cond ((< k n-new-messages)
		       (set! n-added (+ 1 n-added))
		       (send-message! message-queue message
				      #:notify-sent!
				      (make-notify-sent! (- n-added 1)))
		       (loop2 (+ k 1)))
		      (#true (loop remaining)))))
	     (()
	      (put-message server-channel 'stop)
	      #true)))) ; done!
       ;; yield-current-task in a loop only works when singly-threaded.
       ;; The code manipulating the counters above plays a bit loose with concurrency
       ;; concerns, hence both #:hz 0 and #:parallelism 1 is required to avoid
       ;; potential false positives.
       #:hz 0
       #:parallelism 1)))))

;; TODO: extend test to multiple channels, making sure the destroy is sent
;; for the right channel.
(test-assert
 "msg:cadet:local:channel:destroy is sent when closing channels"
 (let ((channel-number 'not-yet)
       (created-condition (make-condition))
       (closed-condition (make-condition)))
   (call-with-services/fibers
    `(("cadet" .
       ,(lambda (port spawn-fiber)
	  (define created? #false)
	  (define closed? #false)
	  (define message-handler/channel-create
	    (message-handler
	     (type (symbol-value message-type msg:cadet:local:channel:create))
	     ((interpose exp) exp)
	     ((well-formed? message)
	      (assert (not created?))
	      (assert (not closed?))
	      (= (slice-length message)
		 (sizeof /:msg:cadet:local:channel:create '())))
	     ((handle! message)
	      ;; TODO: options
	      (let^ ((<-- (address channel-number* options)
			  (analyse-local-channel-create message)))
		    (set! created? #true)
		    (set! channel-number channel-number*)
		    (assert (equal? address address0))
		    (signal-condition! created-condition)
		    (values)))))
	  (define message-handler/channel-destroy
	    (message-handler
	     (type (symbol-value message-type msg:cadet:local:channel:destroy))
	     ((interpose exp) exp)
	     ((well-formed? message)
	      (assert created?)
	      ;; Shouldn't be sent twice -- the GNUnet-Scheme API is idempotent,
	      ;; but the client↔service communication is not.
	      (assert (not closed?))
	      #true)
	     ((handle! message)
	      (let^ ((! channel-number* (analyse-local-channel-destroy message)))
		    (set! closed? #true)
		    ;; Make sure the _right_ channel is closed.
		    (assert (= channel-number channel-number*))
		    (set! closed? #true)
		    (signal-condition! closed-condition)
		    (values)))))
	  (define message-queue
	    (port->message-queue port
				 (message-handlers
				  message-handler/channel-create
				  message-handler/channel-destroy)
				 no-error-handler
				 #:spawn spawn-fiber))
	  (values))))
    (lambda (config spawn-fiber)
      (define server (connect config #:spawn spawn-fiber))
      (define channel
	(open-channel! server address0 (message-handlers)))
      (wait created-condition)
      (close-channel! channel)
      (wait closed-condition)
      #true))))

(test-end "CADET")
